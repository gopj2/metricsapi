<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class MetricController extends Controller
{
    /**
     * @param Request $request
     * @param $param
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function postNewMetric(Request $request, $param)
    {
        $request_value = $request->input("value");

        if ($request_value === null || $request_value === 0) {
            return response()->json([
                "status" => 400,
                "error" => "Bad request"
            ], 400);
        }

        //Get file content
        $file = Storage::disk('local')->exists('metrics.json') ? json_decode(Storage::disk('local')->get('metrics.json'), true) : [];

        //Create new array for more comfortable work with it
        $input_data['parameter'] = $param;
        $input_data['value'] = round($request->input("value"));
        $input_data['date'] = date('Y-m-d H:i:s');
        array_push($file, $input_data);

        //Save new information in file
        Storage::disk('local')->put('metrics.json', json_encode($file, JSON_PRETTY_PRINT));
        return response()->json(null, 200);
    }

    /**
     * @param $param
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function getMetricSum($param)
    {
        //Get file content
        $file = Storage::disk('local')->exists('metrics.json') ? json_decode(Storage::disk('local')->get('metrics.json'), true) : [];

        //Check if file is empty - return 404
        if ($file === null || empty($file)) {
            //Create empty file
            Storage::disk('local')->put('metrics.json', json_encode($file, JSON_PRETTY_PRINT));
            return response()->json([
                "status" => 404,
                "error" => "Resource not found"
            ], 404);
        }

        //Make the json file clear of expires dates (more than 2 hours ago)
        $cleaned_array = $this->checkDateAndRewrite($file);

        //Sum values that we need for request
        $summed = $this->sumValues($cleaned_array, $param);

        //Check if values are exist in the json file
        if ($summed['value'] === 0) {
            return response()->json([
                "status" => 404,
                "error" => "Resource not found"
            ], 404);
        }

        return response()->json($summed, 200);
    }

    /**
     * @param $array
     * @return array
     */
    private function checkDateAndRewrite($array)
    {
        if ($array === null || empty($array)) {
            return [];
        }

        foreach ($array as $index => $item) {
            if (strtotime($item['date']) < (time() - 3600)) {
                unset($array[$index]);
            }
        }

        Storage::disk('local')->put('metrics.json', json_encode($array, JSON_PRETTY_PRINT));

        return $array;
    }

    /**
     * @param $array
     * @param $key
     * @return array
     */
    private function sumValues($array, $key)
    {

        if ($array === null || empty($array)) {
            return [];
        }

        $searched_array['value'] = 0;

        foreach ($array as $index => $item) {
            if ($item['parameter'] === $key) {
                $searched_array['value'] += $item['value'];
            }
        }

        return $searched_array;
    }
}
