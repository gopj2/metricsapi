<?php

Route::post('/metric/{key}', 'MetricController@postNewMetric');
Route::get('metric/{key}/sum', 'MetricController@getMetricSum');
